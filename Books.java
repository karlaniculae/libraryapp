package LibraryBooksManagementApplication;

public class Books {
	String name;
	double price;
	String date;
	Authors author;
	int quantity;
	String genre;

	public Books(String name, double price, String date, Authors author, int quantity, String genre) {
		this.name = name;
		this.price = price;
		this.date = date;
		this.author = author;
		this.quantity = quantity;
		this.genre = genre;
	}
}
